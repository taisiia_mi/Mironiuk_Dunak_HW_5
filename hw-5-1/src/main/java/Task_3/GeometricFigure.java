package Task_3;
public abstract class GeometricFigure {
public abstract double getArea( double side);
public abstract double getPerimeter(double side);
}
